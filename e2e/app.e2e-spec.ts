import { CashytTriviaBackofficePage } from './app.po';

describe('cashyt-trivia-backoffice App', () => {
  let page: CashytTriviaBackofficePage;

  beforeEach(() => {
    page = new CashytTriviaBackofficePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
