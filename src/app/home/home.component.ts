import { Component, OnInit } from '@angular/core';
import { QuestionsService } from '../questions.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private questionsService: QuestionsService) {}

  ngOnInit() {
    this.questionsService.get()
    .subscribe(
      data => {
        console.log(data);
      },
      error => {
        console.log(error);
      });;
  }

}
