import { Component, OnInit } from '@angular/core';
import { Login } from '../models/login';
import { AuthService } from './auth.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  model = new Login('birdy_bytes_team@cashyt.com', '(safePassw0rd)f');

  constructor(private authService: AuthService, private router: Router) {
  }

  ngOnInit() {

  }

  onSubmit() {
    console.log("submitted");

    this.authService.login(this.model.username, this.model.password)
      .subscribe(
      data => {
        this.router.navigate(["home"]);
      },
      error => {
        console.log(error);
      });

  }

}
