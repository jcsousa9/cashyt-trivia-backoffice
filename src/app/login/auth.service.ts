import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { tokenNotExpired } from 'angular2-jwt';

import 'rxjs/add/operator/map'

const AUTH_API: string = 'https://http-dev.cashyt.com/oauth/token'

let options = new RequestOptions({
  headers: new Headers({
    'Content-Type': 'application/x-www-form-urlencoded',
    Authorization: 'Basic M2NmZWNhYjhhYjFiNDQ3N2E0ZGVhNmU5MTUzODJhMDo=',
  })
});

@Injectable()
export class AuthService {

  // store the URL so we can redirect after logging in
  redirectUrl: string;

  constructor(private http: Http) { }

  login(username, password) {
    let body = `grant_type=password&password=${password}&username=${username}`;
    return this.http.post(AUTH_API, body, options)
      .map((response: Response) => {
        // login successful if there's a jwt token in the response
        let objResponse = response.json();

        localStorage.setItem('accessToken', objResponse.access_token);

      });
  }

  logout(): void {
  }

  loggedIn() {
    return tokenNotExpired();
  }
}
