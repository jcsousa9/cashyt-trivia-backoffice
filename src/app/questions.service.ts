import { Injectable } from '@angular/core';
import { RequestOptions, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Question } from './models/question';
import { AuthHttp } from 'angular2-jwt';

import 'rxjs/add/operator/map'

const QUESTIONS_API: string = 'https://http-dev.cashyt.com/quiz/categories';

let options = new RequestOptions({
  headers: new Headers({
    'Content-Type': 'application/json',
  })
});


@Injectable()
export class QuestionsService {

  constructor(private http: AuthHttp) { }

  get(): Observable<Question[]> {

    var token = localStorage.getItem("accessToken");
    options.headers.append("Authorization", "Bearer " + token); // change to be applied in all requests

    return this.http
      .get(QUESTIONS_API, options)
      .map((response: Response) => {
        return response.json();
      });
  }

}
